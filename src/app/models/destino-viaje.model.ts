export class DestinoViaje {
  selected: boolean;
  servicios: string[];
  constructor(public nombre: string, public u: string, public votes: number = 0) {
    this.servicios = ['pileta', 'desayuno'];
  }

  setSelected(s: boolean) {
    this.selected = s;
  }
  isSelected(): boolean {
    return this.selected;
  }
  voteUp() {
    this.votes++;
  }
  voteDown() {
    this.votes--;
  }
}

